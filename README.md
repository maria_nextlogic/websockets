WHA Update systembar in real time 
INTRODUCTION
------------
The WHA systembar displays some important access point of the webpage. The purpose is to update in real time if one of those access points are updated. The main task is to create a client-server communication to update systembar in realtime.


THE CONCEPT
------------
In principle, a connection must be created between js client, which is the WHA website, and a php Websocket  server. In this case it is neccesary for server to make broadcast to a php client. This php client is connect with php server, and through this, the communication between js client and php server will be make.
 
COMPONENTS
------------
[DIR]  /classes/WebsocketService
The directory where are stored php client and php server.

[FILE] /classes/WebsocketService/Websocket-Client.php
The PHP Websocket client file which can be loaded anywhere in WHA System. The php client is located in Websocket-Client.php. This contain a Client class with one method which will be called when a message needs to be sent. More exactly, the Client class will be instantiated in every function where it is necessary to make the change in real time. Also, that message will be send to the php Websocket server. 

[FILE] /classes/WebsocketService/libraries/Client.php 
A PHP Websocket Client library used to initiate the connection between PHP Client and PHP Server.

[FILE] /classes/WebsocketService/Websocket-Server.php
The php server is located in Websocket-Server.php. The PHP Webocket Server works like a daemon and is executed by the following command: php Websocket-Server.php. The server  will be creating a continous communication and wait for a connection with the JS client. It's receiving data from PHP Client and pushing it to all connected JS Websocket Clients.
For running the server properly, it was needed a library wbsockets which is stored in /includes folder.

[DIR]  /includes/wbsockets 
PHP Websocket Server library used for serving WebSockets. It is included/loaded in Websocket-Server.php file.
In /includes/wbsockets/vendor/srchea/php-push-web-socket/src/PushWebSocket directory there is the Server class which contain methods neccesary for server to work. When running the Websocket-Server.php file the server is open by calling runws() method from Server class. 

JS Client
This client is the WHA website. In the first place the server was not capable to be connected with this js client. So, it was neccesary a alternative route. This is proxying our php socket server through the nginx server with the proper paraments.

Nginx config file:
/etc/nginx/sites-enabled/wha

Added the following code snippet
	location /wss {
	proxy_pass             http://127.0.0.1:8082;
	proxy_read_timeout     60;
	proxy_connect_timeout  60;
	proxy_redirect         off;

	# Allow the use of websockets
	proxy_http_version 1.1;
	proxy_set_header Upgrade $http_upgrade;
	proxy_set_header Connection 'upgrade';
	proxy_set_header Host $host;
	proxy_cache_bypass $http_upgrade;
}
[FILE] /js/it_system_bar.js
A JS file where the Websocket object is created. The client is able to communicate with server using Websocket protocol. This Websocket object will automatically attempt to open the connection to the server.
When a message is received, a message event is sent to the Websocket object. To handle it, it's used the onmessage event.

 If the server is turned on, whenever a user accesses the website, a communication appears between the JS client and the server. The php client is connect only on that access point on the js client where the Client class is load and the server need to make an update in real time.