
<?php

error_reporting(E_ALL);


require_once dirname(__DIR__, 2) . '/includes/wbsockets/vendor/autoload.php'; // Autoload files using Composer autoload

set_time_limit(0);

// variables
$address = '127.0.0.1';
$port = 8082;
$verboseMode = true;

$wsserver = new \PushWebSocket\Server($address, $port, $verboseMode);

$wsserver->runws();
//$wsserver->run_websocket();
//  $wsserver->run(); 
            
?>
