$(document).ready(function() {
    
    $(".chosen").chosen({width:'100%'});

    $('.it_sys_nav').on('click', '.play_icon', function() {
        var software_developer = $('#software_developer').val();
        var current_job_id = $('#current_job_id').val();
        var job_id = $(this).attr("data-job-id");
        var obj = $(this);
        if (current_job_id != "") {
            var newhtml = "";         
            newhtml += "<div id=\'tempdiv\'>";
                newhtml += "</div>";
                var dlg_obg = $('<div></div>').appendTo('body')
                .html(newhtml)
                .dialog({
                modal: true,
                title: 'Start A Job',
                zIndex: 10000,
                autoOpen: true,
                width: '500',
                resizable: true,
                autoResize: false,
                minHeight: 200,
                maxHeight: 500,
                buttons: {
                    "Cancel" : {
                        text: "No",
                        class: 'btn btn-danger btn-sm',
                        id: "start_job_with_current_job_dlg_cancel_btn",
                        click: function () {
                            $(this).dialog("close");
                        }
                    },
                    "Save": {
                        text: "Yes",
                        class: 'btn btn-success btn-sm',
                        id: "start_job_with_current_job_dlg_save_btn",
                        click: function() {
                            var dlg = $(this);
                            var save_btn = $("#start_job_with_current_job_dlg_save_btn");
                            save_btn.prop('disabled', true).append('<div class="spinner-border spinner-border-sm ml-2 start_job_with_current_job_dlg_spinner"></div>');

                            $.post("/classes/Projects/wha_job_functions.php", {
                                action: "start_job_with_current_job",
                                current_job_id: current_job_id, 
                                job_id: job_id
                            }, function (data, status, jqXHR) {
                                try {
                                    var response = $.parseJSON(data);
                                    if(response.success == "true") {
                                        change_active_job(job_id, current_job_id);
                                        //stopping current job's table
                                        $('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.total_time_list').removeClass('d-none');
                                        $('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.total_time_list').css("display","");
                                        if($('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.member_time_worked[data-member_id="'+(response.member_id)+'"]').length) {
                                            $('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.member_time_worked[data-member_id="'+(response.member_id)+'"]').html(response.total_time_list_tr_tds_cj);
                                        } else {
                                            $('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.total_time_list').find('tbody').append(response.total_time_list_tr_cj);
                                        }
                                        $('.job_details_other_div[data-job-id="'+current_job_id+'"]').find('.total_time_list').find('tfoot').html(response.total_time_list_footer_cj);

                                        //starting job's table
                                        if(!$('.job_details_other_div[data-job-id="'+job_id+'"]').find('.total_time_list').length) {
                                            $('.job_details_other_div[data-job-id="'+job_id+'"]').html(response.table_structure);
                                        }
                                        $('.job_details_other_div[data-job-id="'+job_id+'"]').find('.total_time_list').removeClass('d-none');
                                        $('.job_details_other_div[data-job-id="'+job_id+'"]').find('.total_time_list').css("display","");
                                        if($('.job_details_other_div[data-job-id="'+job_id+'"]').find('.member_time_worked[data-member_id="'+(response.member_id)+'"]').length) {
                                            $('.job_details_other_div[data-job-id="'+job_id+'"]').find('.member_time_worked[data-member_id="'+(response.member_id)+'"]').html(response.total_time_list_tr_tds_j);
                                        } else {
                                            $('.job_details_other_div[data-job-id="'+job_id+'"]').find('.total_time_list').find('tbody').append(response.total_time_list_tr_j);
                                        }
                                        $('.job_details_other_div[data-job-id="'+job_id+'"]').find('.total_time_list').find('tfoot').html(response.total_time_list_footer_j);

                                        $('#current_job_id').val(job_id);
                                        $(dlg).dialog("close");
                                    }
                                    else {
                                        save_btn.prop('disabled', false);
                                        $(".start_job_with_current_job_dlg_spinner").remove();
                                    }
                                } 
                                catch(err) {
                                    console.log(err);
                                    save_btn.prop('disabled', false);
                                    $(".start_job_with_current_job_dlg_spinner").remove();
                                    alert("Error: Unable to start a job.");
                                }
                            });
                        }
                    }
                },
                close: function (event, ui) {
                    $(this).remove();
                }
            }).css({
                "overflow" : "visible"
            });
            
            $.post('/classes/Projects/wha_job_functions.php', {
                action: 'load_start_job_with_current_job_dialog',
                current_job_id: current_job_id,
                job_id: job_id
            }, function (data, status, jqXHR) {
                try {
                    var response = $.parseJSON(data);
                    if (response.success == 'true') {
                        $("#tempdiv").html(response.form);
                    } else {
                        console.log(data);
                        alert("An error occurred.");
                    }
                }
                catch (err) {
                    console.log(data);
                    console.log(err);
                    alert("A system error occurred.");
                }
            });
        } else if (current_job_id == "") {
            $.post('/classes/Projects/wha_job_functions.php', {
                action: 'start_job_without_current_job',
                current_job_id: current_job_id,
                job_id: job_id
            }, function (data, status, jqXHR) {
                try {
                    var response = $.parseJSON(data);
                    if (response.success == 'true') {
                        if($('.play_pause_div[data-job-id="'+job_id+'"]').length) {
                            $('.play_pause_div[data-job-id="'+job_id+'"]').find('.play_icon').addClass("d-none");
                            $('.play_pause_div[data-job-id="'+job_id+'"]').find('.pause_icon').removeClass("d-none");
                        }

                        $('#current_job_id').val(job_id);
                    } else {
                        console.log(data);
                        alert("An error occurred.");
                    }
                }
                catch (err) {
                    console.log(data);
                    console.log(err);
                    alert("A system error occurred.");
                }
            });
        }
    });

    $('.it_sys_nav').on('click', '.pause_icon', function() {
        var software_developer = $('#software_developer').val();
        // var current_job_id = $('#current_job_id').val();
        var job_id = $(this).attr("data-job-id");
        var current_job_id = $(this).attr("data-job-id");

        $.post('/classes/Projects/wha_job_functions.php', {
            action: 'pause_job',
            current_job_id: current_job_id,
            job_id: job_id
        }, function (data, status, jqXHR) {
            try {
                var response = $.parseJSON(data);
                if (response.success == 'true') {
                    if($('.play_pause_div[data-job-id="'+job_id+'"]').length) {
                       $('.play_pause_div[data-job-id="'+job_id+'"]').find('.play_icon').removeClass("d-none");
                        $('.play_pause_div[data-job-id="'+job_id+'"]').find('.pause_icon').addClass("d-none"); 
                    }
                    
                    $('#current_job_id').val("");
                } else {
                    console.log(data);
                    alert("An error occurred.");
                }
            }
            catch (err) {
                console.log(data);
                console.log(err);
                alert("A system error occurred.");
            }
        }); 
    });

    $(document).on('change', '#select_job_id', function() {
        var software_developer = $('#software_developer').val();
        var current_job_id = $('#current_job_id').val();
        var job_id = $(this).val();
        var obj = $(this);
        if (current_job_id != "") {
            var newhtml = "";         
            newhtml += "<div id=\'tempdiv\'>";
                newhtml += "</div>";
                var dlg_obg = $('<div></div>').appendTo('body')
                .html(newhtml)
                .dialog({
                modal: true,
                title: 'Start A Job',
                zIndex: 10000,
                autoOpen: true,
                width: '500',
                resizable: true,
                autoResize: false,
                minHeight: 200,
                maxHeight: 500,
                buttons: {
                    "Cancel" : {
                        text: "No",
                        class: 'btn btn-danger btn-sm',
                        id: "start_job_with_current_job_dlg_cancel_btn",
                        click: function () {
                            $(this).dialog("close");
                            $("#select_job_id").val(current_job_id).trigger("chosen:updated");
                        }
                    },
                    "Save": {
                        text: "Yes",
                        class: 'btn btn-success btn-sm',
                        id: "start_job_with_current_job_dlg_save_btn",
                        click: function() {
                            var dlg = $(this);
                            var save_btn = $("#start_job_with_current_job_dlg_save_btn");
                            save_btn.prop('disabled', true).append('<div class="spinner-border spinner-border-sm ml-2 start_job_with_current_job_dlg_spinner"></div>');

                            $.post("/classes/Projects/wha_job_functions.php", {
                                action: "start_job_with_current_job",
                                current_job_id: current_job_id, 
                                job_id: job_id
                            }, function (data, status, jqXHR) {
                                try {
                                    var response = $.parseJSON(data);
                                    if(response.success == "true") {

                                        $(".it_sys_nav").find(".play_pause_div").attr("data-job-id", job_id).removeClass("d-none");
                                        $(".it_sys_nav").find(".play_icon").attr("data-job-id", job_id).addClass("d-none");
                                        $(".it_sys_nav").find(".pause_icon").attr("data-job-id", job_id).removeClass("d-none");

                                        $('#current_job_id').val(job_id);
                                        $(dlg).dialog("close");
                                    }
                                    else {
                                        save_btn.prop('disabled', false);
                                        $(".start_job_with_current_job_dlg_spinner").remove();
                                    }
                                } 
                                catch(err) {
                                    console.log(err);
                                    save_btn.prop('disabled', false);
                                    $(".start_job_with_current_job_dlg_spinner").remove();
                                    alert("Error: Unable to start a job.");
                                }
                            });
                        }
                    }
                },
                close: function (event, ui) {
                    
                    $(this).remove();
                }
            }).css({
                "overflow" : "visible"
            });
            
            $.post('/classes/Projects/wha_job_functions.php', {
                action: 'load_start_job_with_current_job_dialog',
                current_job_id: current_job_id,
                job_id: job_id
            }, function (data, status, jqXHR) {
                try {
                    var response = $.parseJSON(data);
                    if (response.success == 'true') {
                        $("#tempdiv").html(response.form);
                    } else {
                        console.log(data);
                        alert("An error occurred.");
                    }
                }
                catch (err) {
                    console.log(data);
                    console.log(err);
                    alert("A system error occurred.");
                }
            });
        } else if (current_job_id == "") {
            $.post('/classes/Projects/wha_job_functions.php', {
                action: 'start_job_without_current_job',
                current_job_id: current_job_id,
                job_id: job_id
            }, function (data, status, jqXHR) {
                try {
                    var response = $.parseJSON(data);
                    if (response.success == 'true') {
                        $(".it_sys_nav").find(".play_pause_div").attr("data-job-id", job_id).removeClass("d-none");
                        $(".it_sys_nav").find(".play_icon").attr("data-job-id", job_id).addClass("d-none");
                        $(".it_sys_nav").find(".pause_icon").attr("data-job-id", job_id).removeClass("d-none");

                        $('#current_job_id').val(job_id);
                    } else {
                        console.log(data);
                        alert("An error occurred.");
                    }
                }
                catch (err) {
                    console.log(data);
                    console.log(err);
                    alert("A system error occurred.");
                }
            });
        }
    });
    
    function initSocket() {
        
       var ws, url = 'wss://wha.nextlogic.ro/wss'; 

//        window.onbeforeunload = function() { 
//                ws.send('quit');
//        };

//        window.onload = function() {
                try {
                        ws = new WebSocket(url);
                        write('Connecting... (readyState '+ws.readyState+')');
                        ws.onopen = function(msg) {
                                write('Connection successfully opened (readyState ' + this.readyState+')');
                        };
                        ws.onmessage = function(msg) {
                            console.log("msg data", msg.data);
                            if(typeof msg.data !== "undefined" && msg.data !== "") {
                                try{
                                   var serverData = JSON.parse(msg.data);
                                   console.log("serverData##", serverData)
                                   var c =  $(".fas.fa-pause").next().html();
                                   console.log("c", c);
                                   if(c) {
                                       var newCount = parseInt(c) - 1;
                                       console.log("---new count----", newCount);
                                       $(".fas.fa-pause").next().html(newCount);
                                   }
                                   console.log("JS Client received data : ", serverData);
                                   console.log( $(".fas.fa-pause .badge") );
                                }catch(err){
                                    console.log("decode error", err);
                                }
                            }
//                            console.log("--------------msg-----------", msg);
//                            alert("msg.data");
                            write('Server says: '+msg.data);
                        };
                        ws.onclose = function(msg) {
                                if(this.readyState == 2)
                                        write('Closing... The connection is going throught the closing handshake (readyState '+this.readyState+')');
                                else if(this.readyState == 3)
                                        write('Connection closed... The connection has been closed or could not be opened (readyState '+this.readyState+')');
                                else
                                        write('Connection closed... (unhandled readyState '+this.readyState+')');
                        };
                        ws.onerror = function(event) {
//                                terminal.innerHTML = '<li style="color: red;">'+event.data+'</li>'+terminal.innerHTML;
                        };
                }
                catch(exception) {
                        write(exception);
                }
//        };

        function write(text) {
//                var date = new Date();
//                var dateText = '['+date.getFullYear()+'-'+(date.getMonth()+1 > 9 ? date.getMonth()+1 : '0'+date.getMonth()+1)+'-'+(date.getDate() > 9 ? date.getDate() : '0'+date.getDate())+' '+(date.getHours() > 9 ? date.getHours() : '0'+date.getHours())+':'+(date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes())+':'+(date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds())+']';
//                var terminal = document.getElementById('terminal');
//                terminal.innerHTML = '<li>'+dateText+' '+text+'</li>'+terminal.innerHTML;
        } 
    }
    
    initSocket();

});
